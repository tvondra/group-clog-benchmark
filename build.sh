echo "machine workload sync patch run clients tps"

for s in e5-2620-v4-2x e5-4620-v1-4x; do

	if [ "$s" == "e5-2620-v4-2x" ]; then
		runs=5
		clients="1 4 8 16 32 64"
	else
		runs=10
		clients="16 32 64 128 192"
	fi

	for t in dilip pgbench; do

		for x in sync-commit-on sync-commit-off; do

			for d in pg-9.6-master pg-9.6-group-update pg-9.6-granular-locking pg-9.6-granular-locking-no-content-lock; do

				for r in `seq 1 $runs`; do

					for c in $clients; do

						tps=`cat $s/$t/$x/$d/clients-$c-$r.log | grep excluding | awk '{print $3}'`

						echo $s $t $x $d $r $c $tps

					done

				done

			done

		done

	done

done

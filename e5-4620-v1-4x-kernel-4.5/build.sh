echo "machine workload sync patch run clients tps"

runs=10
clients="1 8 16 32 64 128 192"

for t in dilip pgbench; do

	for d in pg-9.6-master pg-9.6-group-update pg-9.6-granular-locking pg-9.6-granular-locking-no-content-lock; do

		for r in `seq 1 $runs`; do

			for c in $clients; do

				tps=`cat $t/$d/clients-$c-$r.log | grep excluding | awk '{print $3}'`

				echo $t $d $r $c $tps

			done

		done

	done

done
